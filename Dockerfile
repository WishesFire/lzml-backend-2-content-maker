FROM python:3.9

WORKDIR E://lzml-content

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

COPY ./requirements.txt E://lzml-content/requirements.txt

RUN set
 && pip install -r /usr/lzml-content/requirements.txt

COPY . /usr/lzml-content

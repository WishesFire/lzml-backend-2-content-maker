import uvicorn
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from starlette.middleware.sessions import SessionMiddleware
from src.db.create_db import init_database
from src.endpoints.routers import api_router
from src.configs import config


def create_application() -> FastAPI:
    application = FastAPI(title='Lzml contents maker',
                          description='FastAPI application created for making contents for Lzml forum',
                          version='v0.0.1')
    application.add_middleware(
        CORSMiddleware,
        allow_origins=[
            "http://localhost",
            "http://localhost:4200",
            "http://localhost:3000",
            "http://localhost:8080",
        ],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"])
    application.add_middleware(SessionMiddleware, secret_key=config.SECRET_KEY)
    application.include_router(api_router, prefix=config.API_V1_STR)
    init_database(app=application)

    return application


if __name__ == "__main__":
    app = create_application()
    uvicorn.run(app, host=config.SERVER_HOST, port=int(config.SERVER_PORT), debug=True)

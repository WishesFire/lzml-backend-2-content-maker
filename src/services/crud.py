from typing import TypeVar, Type, Optional
from tortoise import models
from tortoise.transactions import atomic
from pydantic import BaseModel
from collections.abc import Sequence
from random import randint


T = TypeVar('T')
ModelType = TypeVar("ModelType", bound=models.Model)
CreateSchemaType = TypeVar("CreateSchemaType", bound=BaseModel)
UpdateSchemaType = TypeVar("UpdateSchemaType", bound=BaseModel)
GetSchemaType = TypeVar("GetSchemaType", bound=BaseModel)
DeleteSchemaType = TypeVar("DeleteSchemaType", bound=BaseModel)

get_schema: GetSchemaType


async def create(model: Type[ModelType], schema: Type[CreateSchemaType], **kwargs) -> Optional[CreateSchemaType]:
    obj = await model.create(**schema.dict(exclude_unset=True), **kwargs)
    return get_schema.from_toroise_orm(obj)


async def get__all(model: Type[ModelType], schema: Type[GetSchemaType]) -> Sequence[T]:
    return await schema.from_queryset(model.all())


async def get__single(model: Type[ModelType], schema: Type[GetSchemaType], **kwargs) -> Optional[GetSchemaType]:
    return await schema.from_queryset_single(model.get(**kwargs))


async def get__review_from_category(model: Type[ModelType], schema: Type[GetSchemaType],
                                    **kwargs) -> Optional[GetSchemaType]:
    obj = await model.get(**kwargs)
    return schema.from_queryset(obj.reviews)


@atomic
async def get__random_single(model: Type[ModelType], schema: Type[GetSchemaType]) -> Optional[GetSchemaType]:
    random_index = 0
    if model.__class__.__name__ == "AnimeModel":
        random_max_id = await model.random_choice()
        random_index = randint(1, random_max_id[0].max_id - 1)
    return await schema.from_queryset_sinle(model.filter(id=random_index).first())

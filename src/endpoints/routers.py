from fastapi import APIRouter
from ..endpoints.reviews.comments import comment_router
from ..endpoints.reviews.rating import rating_router
from ..endpoints.reviews.base_reviews import reviews_router
from ..endpoints.base import base_router
from ..endpoints.news.base_news import news_router
from ..endpoints.news.tags import tags_router
from ..endpoints.elements.anime import anime_router


api_router = APIRouter()

# Reviews
api_router.include_router(comment_router, prefix='', tags=["data"])
api_router.include_router(rating_router, prefix='', tags=["data"])
api_router.include_router(reviews_router, prefix='', tags=["data"])
# Base
api_router.include_router(base_router, prefix='', tags=["data"])
# News
api_router.include_router(news_router, prefix='', tags=["data"])
api_router.include_router(tags_router, prefix='', tags=["data"])
# Categories
api_router.include_router(anime_router, prefix='', tags=["data"])

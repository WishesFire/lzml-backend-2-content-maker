from fastapi import APIRouter

base_router = APIRouter()


@base_router.get("/ping", status_code=200)
async def check_connection() -> dict:
    return {"Ping": "Pong"}

from fastapi import APIRouter, Depends
from fastapi_pagination import Page, Params, paginate
from fastapi_cache import FastAPICache
from fastapi_cache.backends.memcached import MemcachedBackend
from fastapi_cache.decorator import cache
from aiomcache import Client
from src.parsing.categories.schemas import AnimeSerializer, get_anime_pydantic
from src.parsing.categories.models import AnimeModel
from ...models.schemas import ReviewSerializer, review_pydantic
from src.services.crud import (get__all,
                               get__single,
                               get__review_from_category,
                               get__random_single)

anime_router = APIRouter()


@anime_router.on_event("startup")
async def startup():
    FastAPICache.init(MemcachedBackend(Client("localhost", 8000)), prefix="fastapi-cache")


@anime_router.get("/anime", response_model=Page[AnimeSerializer], status_code=200)
@cache(expire=60)
async def get_all_anime(params: Params = Depends()) -> get_anime_pydantic:
    return paginate(await get__all(model=AnimeModel, schema=get_anime_pydantic), params)


@anime_router.get("/anime/{anime_id}", response_model=AnimeSerializer, status_code=200)
async def get_single_anime(anime_id: int) -> get_anime_pydantic:
    return await get__single(model=AnimeModel, schema=get_anime_pydantic, object_id=anime_id)


@anime_router.get("/anime/{anime_id}/reviews", response_model=ReviewSerializer, status_code=200)
async def get_reviews_anime(anime_id: int) -> review_pydantic:
    return await get__review_from_category(model=AnimeModel, schema=get_anime_pydantic, object_id=anime_id)


@anime_router.get("anime/random", response_model=AnimeSerializer, status_code=201)
async def get_random_anime() -> get_anime_pydantic:
    return await get__random_single(model=AnimeModel, schema=get_anime_pydantic)



async def get_client_ip(request):
    x_forwarded_for = request.Meta.get("HTTP_X_FORWARDED_FOR")
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.Meta.get("REMOTE_ADDR")
    return await ip

from fastapi import APIRouter, HTTPException, Depends, Request
from fastapi_pagination import Page, Params, paginate
from tortoise.contrib.fastapi import HTTPNotFoundError
from src.models.reviews import ReviewModel, IpUserModel
from src.models.schemas import ReviewSerializer, ShowReviewSerializer, \
    review_pydantic, review_pydantic_no_ids
from src.endpoints.services.ip import get_client_ip


reviews_router = APIRouter()


@reviews_router.get("/reviews", response_model=Page[ShowReviewSerializer], status_code=200)
async def all_anime_reviews(params: Params = Depends()) -> review_pydantic:
    return paginate(await review_pydantic.from_queryset(ReviewModel.all()), params)


@reviews_router.get("/reviews/{review_id}", response_model=ReviewSerializer, status_code=200)
async def specific_anime_reviews(review_id: int, request: Request) -> ReviewSerializer:
    user_ip = await get_client_ip(request=request)
    review = await ReviewModel.get(id=review_id)
    ip_ex = await IpUserModel.get(ip=user_ip)

    if ip_ex:
        await review.views.add(ip_ex)
    else:
        await IpUserModel.create(ip=user_ip)
        await review.views.add(await IpUserModel.get(ip=user_ip))

    return await review_pydantic_no_ids.from_queryset_single(review)


@reviews_router.put("/reviews/{review_id}", response_model=review_pydantic,
                    responses={404: {"model": HTTPNotFoundError}})
async def update_review(review_id: int, review: review_pydantic) -> review_pydantic_no_ids:
    await ReviewModel.filter(id=review_id).update(**review.dict(exclude_unset=True))
    return await review_pydantic_no_ids.from_queryset_single(ReviewModel.get(id=review_id))


@reviews_router.delete("/reviews/{review_id}", status_code=202)
async def delete_review(review_id: int) -> dict:
    deleted_job = await ReviewModel.filter(id=review_id).delete()
    if not deleted_job:
        raise HTTPException(status_code=404, detail=f"Review {review_id} not found")
    return {"msg": f"{review_id} successfully deleted"}


@reviews_router.post("/review_create", status_code=201)
async def create_review(review: ReviewSerializer) -> dict:
    try:
        new_review = await ReviewModel.create(**review.dict(exclude_unset=True))
        return {
            "msg": "Successfully created new review",
            "Review ID": new_review.id,
            "Name review": new_review.title
            }
    except Exception as _:
        raise HTTPException(status_code=404, detail="Review don't create")

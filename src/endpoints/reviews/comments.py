from typing import List
from fastapi import APIRouter, HTTPException
from src.models.schemas import CommentChildrenSerializer, create_comment_review_pydantic, \
    comment_review_pydantic
from src.models.reviews import CommentReviewModel


comment_router = APIRouter()


@comment_router.get("/reviews/{review_id}/comments", response_model=List[CommentChildrenSerializer])
async def review_all_comments(review_id: int) -> comment_review_pydantic:
    return await comment_review_pydantic.from_queryset(CommentReviewModel.filter(review_id=review_id))


@comment_router.post("/reviews/{review_id}/set_comment", status_code=201)
async def review_set_comment(review_id: int, schema: create_comment_review_pydantic) -> dict:
    try:
        new_comment = await CommentReviewModel.create(**schema.dict(exclude_unset=True), review_id=review_id)
        return {
            "msg": "Successfully created a review comment",
            "Comment ID": new_comment.id,
            }
    except Exception as _:
        raise HTTPException(status_code=404, detail="Review don't create")


@comment_router.delete("/reviews/{review_id}/{comment_id}", status_code=202)
async def review_delete_comment(review_id: int, comment_id: int) -> dict:
    deleted_comment = await CommentReviewModel.filter(review_id=review_id, id=comment_id).delete()
    if not deleted_comment:
        raise HTTPException(status_code=404, detail=f"Comment {comment_id} not found")
    return {"msg": f"{comment_id} successfully deleted"}

from fastapi import APIRouter
from src.models.reviews import RatingReviewModel
from src.models.schemas import rating_review_pydantic

rating_router = APIRouter()


@rating_router.post("/reviews/{review_id}/set_rating", status_code=201)
async def review_set_rating(review_id: int, schema: rating_review_pydantic) -> dict:
    data = schema.dict()
    await RatingReviewModel.create(review_id=review_id, mark=data["mark"], owner_id=data["owner_id"])
    return {"msg": "Rating successfully set"}

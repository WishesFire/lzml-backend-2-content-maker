from typing import List
from fastapi import APIRouter, HTTPException
from src.models.schemas import get_tag_pydantic, create_tag_pydantic
from src.models.news import TagNewsModel
from src.models.superuser import Admin

tags_router = APIRouter()


@tags_router.post("/tag", response_model=get_tag_pydantic)
async def create_tag(schema: create_tag_pydantic, super_user: int) -> create_tag_pydantic:
    if super_user in [Admin[admin_id] for admin_id in Admin]:
        return await create_tag_pydantic.from_tortoise_orm(TagNewsModel.create(**schema.dict(exclude_unset=True)))
    raise HTTPException(status_code=404, detail=f"User {super_user} is not superuser")


@tags_router.get("/tag", response_model=List[get_tag_pydantic])
async def get_all_tag() -> list:
    return await get_tag_pydantic.from_queryset(TagNewsModel.all())


@tags_router.get("/tag/{pk}", response_model=get_tag_pydantic)
async def get_one_tag(pk: int) -> get_tag_pydantic:
    return await get_tag_pydantic.from_queryset_single(TagNewsModel.get(id=pk))


@tags_router.delete("/tag/{pk}", status_code=204)
async def delete_tag(pk: int, super_user: int) -> None:
    if super_user in [Admin[admin_id] for admin_id in Admin]:
        res = await TagNewsModel.filter(id=pk)
        if not res:
            raise HTTPException(status_code=404, detail='Object does not exist')
    raise HTTPException(status_code=404, detail=f"User {super_user} is not superuser")

from typing import List, Optional
from fastapi import APIRouter, Depends, HTTPException, Query
from fastapi_pagination import Page, Params, paginate
from src.models.schemas import NewsSerializer, news_pydantic
from src.models.news import NewsModel, TagNewsModel
from src.models.superuser import Admin


news_router = APIRouter()


@news_router.get("/news", response_model=Page[NewsSerializer], status_code=200)
async def get_all_news(params: Params = Depends()) -> news_pydantic:
    return paginate(await news_pydantic.from_queryset(NewsModel.all()), params)


@news_router.get("/news/{news_id}", response_model=NewsSerializer, status_code=200)
async def get_detail_news(news_id: int) -> news_pydantic:
    one_news = await NewsModel.get(id=news_id)
    return news_pydantic.from_queryset_single(one_news)


@news_router.post("/news/create", response_model=NewsSerializer, status_code=201)
async def create_news(news: news_pydantic, tags: List[int], super_user: int) -> Optional[NewsSerializer]:
    if super_user in [Admin[admin_id] for admin_id in Admin]:
        obj = await NewsModel.create(**news.dict(exclude_unset=True))
        tags = await TagNewsModel.filter(id__in=tags)
        await obj.tag.add(*tags)
        return news_pydantic.from_tortoise_orm(obj)
    raise HTTPException(status_code=404, detail=f"User {super_user} is not superuser")


@news_router.get("/news/filter", response_model=Page[NewsSerializer], status_code=200)
async def filter_news(
        category: str = "", tag: Optional[List[str]] = Query(None), params: Params = Depends()
        ):
    return paginate(await news_pydantic.from_queryset(
        NewsModel.filter(category__name=category, tag__name=tag).distinct()), params)

import asyncio
import argparse
from collections import namedtuple
from loguru import logger
from src.parsing.categories.category import ParseAPI
from src.parsing.config import CATEGORIES_AVAILABLE
from src.configs.config import DATABASE_URI, DB_NAME_STORAGE
from tortoise import Tortoise, run_async
from aiohttp import ClientError, ClientSession, InvalidURL

TIME_SLEEP = 3060
TOTAL_ERROR = 10
DEBUG = True


async def init_db() -> None:
    await Tortoise.init(db_url=DATABASE_URI + DB_NAME_STORAGE, modules={"models": ["src.parsing.models"]})
    await Tortoise.generate_schemas()


async def main() -> None:
    if not DEBUG:
        parser_args = argparse.ArgumentParser()
        parser_args.add_argument("-a", dest="regime", required=True, help="Regime parser", type=str)
        parser_args.add_argument("-b", dest="categories", required=False, help="Type parsing", type=str)
        arg = parser_args.parse_args()
    else:
        Arg_P = namedtuple("Argument", ["regime", "categories"])
        arg = Arg_P(regime="full", categories="Anime")
    total = 1
    logger.add("info.log", format="{time} {level} {message}",
               level="INFO", rotation="10 KB", compression="zip", enqueue=True)
    try:
        logger.info("Start!!!")
        logger.info(f"Count - {total}")
        tasks = []
        logger.info(f"Regime - {arg.regime} launch")
        if arg.regime == "full":
            if arg.categories and arg.categories in CATEGORIES_AVAILABLE:
                logger.info(f"Mode full type - {arg.categories} launch")
                parser_full = ParseAPI(flag=arg.categories)
                await asyncio.ensure_future(parser_full.full_data())
        elif arg.regime == "update":
            for flag in CATEGORIES_AVAILABLE:
                logger.info(f"Mode update type - {flag} launch")
                parser_update = ParseAPI(flag=flag)
                tasks.append(asyncio.ensure_future(parser_update.update_data()))
            await asyncio.gather(*tasks)

    except (ClientError, ClientSession, InvalidURL) as error:
        logger.error(f"Exception - {error}")
        if total < TOTAL_ERROR and arg.regime != "full":
            total += 1
            await asyncio.sleep(TIME_SLEEP)
            await main()
        else:
            exit(1)

if __name__ == '__main__':
    # For init and migrate db
    run_async(init_db())
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())

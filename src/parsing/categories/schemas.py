from pydantic import BaseModel
from tortoise.contrib.pydantic import pydantic_model_creator
from datetime import datetime
from typing import TypeVar, Dict, Any, List, Union
from .models import AnimeModel

T = TypeVar("T")
JSONType = Union[str, int, float, bool, None, Dict[str, Any], List[Any]]


class AnimeSerializer(BaseModel):
    id: int
    title: str
    description: str
    score: float
    year: datetime
    rating: T
    rank: int
    image: str
    genres: JSONType
    episodes: T


get_anime_pydantic = pydantic_model_creator(AnimeModel, name="Anime")

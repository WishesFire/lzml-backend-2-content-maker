import asyncio
import json
import random
import aioredis
from typing import Optional, Union
from loguru import logger
from aiohttp import ClientSession
from pyuseragents import random as random_useragent
from src.parsing.categories.models import DataBaseCategories
from src.parsing.parser_app import init_db
from src.parsing.config import STORAGE_LAST_ID


HEADERS_TEMPLATE = {
    'Content-Type': 'application/json',
    'User-Agent': random_useragent(),
    'Connection': 'keep-alive',
    'Accept': 'application/json, text/plain, */*',
    'Accept-Language': 'ru',
    'Accept-Encoding': 'gzip, deflate, br'
}


class ParseAPI:
    def __init__(self, flag):
        # Settings
        self.TOTAL_DELAY = 100000
        self.DELAY_BETWEEN_REQUEST = 4
        self.DELAY_MINUTE = 60
        self.FLAG = flag
        self.HOSTS_URL = "https://raw.githubusercontent.com/abionics/attacker/master/hosts.json"
        self.current_url = None
        self.counter_category = 1
        self.json_storage_data = []

        # Database
        self.db = DataBaseCategories()
        self.redis = aioredis.from_url("redis//localhost", decode_responses=True)
        await init_db()

    async def _get_hosts(self) -> json:
        async with ClientSession() as session:
            while True:
                try:
                    async with session.get(self.HOSTS_URL, timeout=3) as resp:
                        data = await resp.json(content_type="text/plain")
                    return data
                except Exception as error:
                    print(error)
                    continue

    @staticmethod
    async def _fix_url(url: str, force_https: bool = False) -> str:
        if not url.startswith("http"):
            url = "http://" + url
        if force_https:
            url = url.replace("http://", "https://")
        return url

    async def _clear_storage_data(self) -> None:
        logger.info("Clean json storage")
        self.json_storage_data.clear()

    async def __get_last_id(self) -> int:
        last_id = await self.redis.get(STORAGE_LAST_ID)
        logger.info(f"Get current id - {last_id}")
        return last_id

    async def __save_last_id(self) -> None:
        logger.info(f"Save current id - {self.counter_category}")
        await self.redis.set(STORAGE_LAST_ID, self.counter_category)

    async def _request_full_site(self, session: ClientSession, proxy_url: str = None) -> int:
        # Per Second - 3 requests
        per_second_request = 0
        try:
            for i in range(self.DELAY_MINUTE):
                update_url = self.current_url + self.counter_category
                async with session.get(update_url, proxy=proxy_url) as resp:
                    if resp.status == 200:
                        all_data = await resp.json()
                        self.json_storage_data.append(all_data)
                        self.counter_category += 1
                        if per_second_request == 3:
                            per_second_request = 0
                            # Regulate delay_between
                            await asyncio.sleep(self.DELAY_BETWEEN_REQUEST)
                    else:
                        return resp.status
            return 200
        except Exception as error:
            logger.warning(f"Exception - {error}")
            await self.__save_last_id()

    async def _request_update_site(self, session: ClientSession) -> (bool, Union[str, None]):
        async with session.get(self.current_url) as resp:
            msg = await resp.text()
            if resp.status == 200:
                status = await self.db.check_database(resp.json())
                if not status:
                    msg = "Current data is exist in database"
                    return True, msg
                status = await self.db.update_save_to_database(resp.json())
                if not status:
                    msg = "Successfully not created in database"
                    return False, msg
                return True
        return False, msg

    async def parser_full_data(self, proxy_flag: Optional[bool] = None) -> None:
        logger.info("Parse full data")
        hosts = await self._get_hosts()
        while True:
            host = random.choice(hosts)
            logger.info(f"Current host - {host}")
            async with ClientSession(headers=HEADERS_TEMPLATE) as session:
                if proxy_flag:
                    async with session.get(host, verify_ssl=False) as resp:
                        content = await resp.text()
                        data = json.loads(content)
                    future_url_proxy = data["site"]["url"]
                    proxy_url = await self._fix_url(future_url_proxy)
                    success = await self._request_full_site(session=session, proxy_url=proxy_url)
                    logger.info(f"Status requests - {success}")
                    if success != 200:
                        proxies = [
                            f'http://{proxy_data["auth"]}@{proxy_data["ip"]}'
                            for proxy_data in data['proxy']
                        ]
                        random.shuffle(proxies)
                        for proxy in proxies:
                            proxy = await self._fix_url(proxy)
                            success = await self._request_full_site(session=session, proxy_url=proxy)
                            await self.__save_last_id()
                            if not success:
                                break
            status = await self.db.full_save_to_database(self.json_storage_data)
            if status:
                await self._clear_storage_data()
            await asyncio.sleep(self.DELAY_MINUTE)
            logger.info("Waiting...")

    async def parse_update_data(self) -> None:
        logger.info("Parse update data")
        self.counter_category = await self.__get_last_id()
        async with ClientSession(headers=HEADERS_TEMPLATE) as session:
            while True:
                success, msg = await self._request_update_site(session=session)
                if success:
                    self.counter_category += 1
                    await asyncio.sleep(self.TOTAL_DELAY)
                else:
                    logger.warning(msg)
                    break
        await self.__save_last_id()

    async def full_data(self) -> None:
        logger.info("Take full data start")
        if self.FLAG == "Anime":
            self.current_url = "https://api.jikan.moe/v4/anime/"
        elif self.FLAG == "Movie":
            self.current_url = ""
        elif self.FLAG == "Game":
            self.current_url = ""
        await self.parser_full_data(proxy_flag=True)

    async def update_data(self) -> None:
        logger.info("Update current data start")
        if self.FLAG == "Anime":
            self.current_url = "https://api.jikan.moe/v4/anime/"
        elif self.FLAG == "Movie":
            self.current_url = ""
        elif self.FLAG == "Game":
            self.current_url = ""
        await self.parse_update_data()

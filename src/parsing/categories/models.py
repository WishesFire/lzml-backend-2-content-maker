import json
from tortoise import fields
from tortoise.exceptions import ValidationError, BaseORMException
from tortoise.models import Model
from tortoise.manager import Manager, QuerySet
from ...models.reviews import ReviewModel
from tortoise.functions import Max


class AnimeManager(Manager):
    def get_queryset(self) -> QuerySet:
        return super(self).get_queryset()

    def random_choice(self):
        return self.get_queryset().all().annotate(max_id=Max("id")).values()


class AnimeModel(Model):
    id = fields.IntField(pk=True)
    title = fields.CharField(max_length=200)
    description = fields.TextField()  # background
    score = fields.FloatField()
    year = fields.DatetimeField()
    rating = fields.CharField(max_length=100)
    rank = fields.IntField(default=0)
    image = fields.CharField(max_length=500)
    genres = fields.JSONField()
    episodes = fields.IntField()
    reviews: fields.ManyToManyRelation["ReviewModel"] = fields.ManyToManyField(
        "src.models.reviews.ReviewModel", through="reviews_anime", null=True, related_name="reviews"
    )

    class Meta:
        table = "anime"
        manager = AnimeManager()

    def __str__(self):
        return self.title


class DataBaseCategories:
    @staticmethod
    async def push(category: json):
        obj = await AnimeModel.create(
            title=category["title"],
            description=category["background"],
            score=category["score"],
            year=category["year"],
            rating=category["rating"],
            rank=category["rank"],
            image=category["images"]["jpg"]["image_ur"],
            genres=category["genres"],
            episodes=category["episodes"]
        )
        return obj

    @staticmethod
    async def full_save_to_database(categories: list) -> bool:
        try:
            await AnimeModel.bulk_create(
                [AnimeModel(title=anime["title"],
                            description=anime["background"],
                            score=anime["score"],
                            year=anime["year"],
                            rating=anime["rating"],
                            rank=anime["rank"],
                            image=anime["images"]["jpg"]["image_ur"],
                            genres=anime["genres"],
                            episodes=anime["episodes"]) for anime in categories]
            )
            return True
        except (BaseORMException, ValidationError) as _:
            return False

    async def update_save_to_database(self, category: json) -> bool:
        try:
            obj = await self.push(category)
            await obj.save()
            return True
        except (BaseORMException, ValidationError) as _:
            return False

    @staticmethod
    async def check_database(category: json):
        return await AnimeModel.exists(title=category["title"])

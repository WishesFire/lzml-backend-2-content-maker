from src.configs.config import MOVIES_API_KEY

CATEGORIES_AVAILABLE = ("Anime", "Movie", "Games")
STORAGE_LAST_ID = "key-last-id"

# Anime Part
# Popular anime
TOP_ANIME = "https://api.jikan.moe/v3/top/anime/1/"
# Get top upcoming anime (PAGE = 1)
TOP_UPCOMING_ANIME = "https://api.jikan.moe/v3/top/anime/1/upcoming"
# Get pictures (PAGE = 1)
PICTURES_ANIME = "https://api.jikan.moe/v3/anime/1/pictures"
# Search by genre (ID GENRE = 1, PAGE = 1)
GENRE_ANIME_SEARCH = "https://api.jikan.moe/v3/genre/anime/1/1"
# Search anime /search/anime?q=Fate/Zero&page=1
ANIME_SEARCH = "https://api.jikan.moe/v3/search/anime"

# Movie Part
# Popular movies
TOP_MOVIE = "https://api.themoviedb.org/discover/movie?sort_by=popularity.desc&" + MOVIES_API_KEY
# Image for films
IMAGE_MOVIE = "https://image.tmdb.org/t/p/w500"
# Search by genre
GENRE_MOVIE = "https://api.themoviedb.org/genre/movie/list" + MOVIES_API_KEY
# Search by name
SEARCH_TERM = ""
NAME_MOVIE = "https://api.themoviedb.org/search/movie?" + MOVIES_API_KEY + f"&query={SEARCH_TERM}"

# Game Part
# Popular games
TOP_GAME = "https://api.rawg.io/api/games"

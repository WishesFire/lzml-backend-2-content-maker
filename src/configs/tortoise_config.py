from .config import DATABASE_URI, APP_MODELS, DB_NAME


TORTOISE_ORM = {
    "connections": {
        "default": DATABASE_URI + DB_NAME
    },
    "apps": {
        "models": {
            "models": APP_MODELS,
            "default_connection": "default",
        }
    }
}

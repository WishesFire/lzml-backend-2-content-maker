from dotenv import load_dotenv
from os import environ

load_dotenv()

# API URLS
ANIME_API = environ.get("ANIME_URL_SOURCE")
MOVIES_API = environ.get("MOVIES_URL_SOURCE")
MOVIES_API_KEY = "api_key=" + environ.get("MOVIES_SOURCE_API_KEY")
GAMES_API = environ.get("GAMES_URL_SOURCE")
GAMES_API_KEY = environ.get("GAMES_URL_SOURCE_KEY")

# Server settings
SERVER_HOST = environ.get("HOST")
SERVER_PORT = environ.get("PORT")
SECRET_KEY = environ.get("SECRET_KEY")
API_V1_STR = "/api/v1"

# Database settings
DB_USERNAME = environ.get("DB_USERNAME")
DB_PASSWORD = environ.get("DB_PASSWORD")
DB_HOST = environ.get("DB_HOST")
DB_PORT = environ.get("DB_PORT")
DB_NAME = environ.get("DB_NAME")
DB_NAME_STORAGE = environ.get("DB_NAME_STORAGE")
DATABASE_URI = f'postgres://{DB_USERNAME}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/'

APP_MODELS = ["src.models.reviews", "src.models.news", "aerich.models"]

from tortoise.contrib.fastapi import register_tortoise
from fastapi import FastAPI
from src.configs.config import DATABASE_URI, APP_MODELS


def init_database(app: FastAPI) -> None:
    register_tortoise(app=app,
                      db_url=DATABASE_URI,
                      modules={"models": APP_MODELS},
                      generate_schemas=True,
                      add_exception_handlers=True)

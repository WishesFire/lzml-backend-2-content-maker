from src.configs.config import MOVIES_API_KEY, GAMES_API_KEY
import requests
import pprint


def show_request(url: str) -> None:
    response = requests.get(url)
    if response.status_code == 200:
        pprint.pprint(response.json())
    else:
        print(response.request)


class TestAnimeAPI:
    def test_popular_api(self) -> None:
        anime_url = "https://api.jikan.moe/v3/top/anime/40/"
        show_request(anime_url)

    def test_single_api(self) -> None:
        anime_url = "https://api.jikan.moe/v4/anime/1"
        show_request(anime_url)


class TestMovieAPI:
    def test_movie_api(self) -> None:
        movie_url = "https://api.themoviedb.org/discover/movie?sort_by=popularity.desc&" + MOVIES_API_KEY
        show_request(movie_url)


class TestGameAPI:
    def test_game_api(self) -> None:
        game_url = "https://api.rawg.io/api/games/" + GAMES_API_KEY
        show_request(game_url)


if __name__ == '__main__':
    anime = TestAnimeAPI()
    anime.test_single_api()

import json
import asyncio
from . import test_app, event_loop
from src.models.reviews import ReviewModel
from fastapi.testclient import TestClient


USER_TEST_ID = None


def test_all_anime_reviews(test_app: TestClient) -> None:
    response = test_app.get("/api/v1/reviews")

    assert response.status_code == 200
    assert response.json()


def test_create_review(test_app: TestClient) -> None:
    global USER_TEST_ID
    test_request_payload = {"title": "something", "description": "test",
                            "name_of_the_type": "test", "personal_rating": 1,
                            "owner_id": 77}
    response = test_app.post("/api/v1/review_create", data=json.dumps(test_request_payload))

    assert response.status_code == 201
    data = response.json()
    assert "Review ID" in data
    review_id = data["Review ID"]

    async def get_review_by_id():
        review = await ReviewModel.get(id=review_id)
        return review

    loop = asyncio.get_event_loop()
    review_obj = loop.run_until_complete(get_review_by_id())

    if review_obj:
        USER_TEST_ID = review_obj.id

    test_response_payload = {"msg": "Successfully created new review",
                             "Review ID": review_obj.id,
                             "Name review": review_obj.title}
    assert response.json() == test_response_payload


def test_bad_rating_create_review(test_app: TestClient) -> None:
    test_request_payload = {"title": "something", "description": "test",
                            "name_of_the_type": "test", "personal_rating": 100,
                            "owner_id": 77}
    response = test_app.post("/api/v1/review_create", data=json.dumps(test_request_payload))

    assert response.status_code != 201


def test_specific_anime_reviews(test_app: TestClient) -> None:
    response = test_app.get(f"/api/v1/reviews/{USER_TEST_ID}")

    assert response.status_code == 200
    assert response.json()["title"] == "something"


def test_no_specific_anime_reviews(test_app: TestClient) -> None:
    response = test_app.get(f"/api/v1/reviews/{USER_TEST_ID}")

    assert response.status_code != 200


def test_update_review(test_app: TestClient) -> None:
    pass


def test_delete_review(test_app: TestClient) -> None:
    response = test_app.delete(f"/api/v1/reviews/{USER_TEST_ID}")

    assert response.status_code == 202
    assert response.json()["msg"] == f"{USER_TEST_ID} successfully deleted"

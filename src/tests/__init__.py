import pytest
from typing import Generator
from fastapi.testclient import TestClient
from tortoise.contrib.test import finalizer
from app import create_application


# Generate client for testing
@pytest.fixture(scope="module")
def test_app() -> Generator:
    app = create_application()
    with TestClient(app=app) as client:
        yield client
    # finalizer()


# For async operation (event_loop.run_until_complete())
@pytest.fixture(scope="module")
def event_loop(test_app: TestClient) -> Generator:
    yield test_app.task.get_loop()

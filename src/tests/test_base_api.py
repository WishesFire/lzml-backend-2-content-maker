from . import test_app
from fastapi.testclient import TestClient


def test_connection(test_app: TestClient) -> None:
    response = test_app.get("/api/v1/ping")
    assert response.status_code == 200
    assert response.json() == {"Ping": "Pong"}



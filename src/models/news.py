from tortoise.models import Model
from tortoise import fields, Tortoise
from src.models.categories import Categories


class TagNewsModel(Model):
    name = fields.CharField(max_length=50, unique=True, null=True)
    news: fields.ManyToManyRelation['NewsModel']

    def __str__(self):
        return self.name


class NewsModel(Model):
    id = fields.IntField(pk=True)
    tag: fields.ManyToManyRelation["TagNewsModel"] = fields.ManyToManyField(
        "news.TagNewsModel", through="news_tag", null=True, related_name="news"
    )
    category = fields.CharEnumField(Categories)
    title = fields.CharField(max_length=500)
    text = fields.TextField()
    publish_at = fields.DatetimeField(auto_now=True)
    image = fields.CharField(max_length=500, null=True)
    viewed = fields.IntField(default=0)
    author = fields.IntField()

    def __str__(self):
        return self.title


Tortoise.init_models(["src.models.news"], "news")

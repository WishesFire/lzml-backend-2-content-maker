from enum import Enum


class Categories(Enum):
    ANIME = 'Anime'
    MOVIE = 'Movie'
    GAMES = 'Games'


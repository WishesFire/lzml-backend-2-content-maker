from tortoise.models import Model
from tortoise import fields, Tortoise
from tortoise.signals import pre_save
from typing import Type, List
from slugify import slugify


class IpUserModel(Model):
    ip = fields.CharField(max_length=100)

    def __str__(self):
        return f"Ip user: {self.ip}"


class ReviewModel(Model):
    id = fields.IntField(pk=True)
    title = fields.CharField(max_length=100)
    slug = fields.CharField(max_length=100, null=True)
    description = fields.TextField(max_length=1500)
    create_at = fields.DatetimeField(auto_now_add=True)
    name_of_the_type = fields.CharField(max_length=500)  # Anime, Movies, Games
    views: fields.ManyToManyRelation['IpUserModel'] = fields.ManyToManyField(
        "reviews.IpUserModel", through="review_view", null=True, related_name="review_views"
    )
    personal_rating = fields.SmallIntField()
    owner_id = fields.IntField()

    def __str__(self):
        return self.title


@pre_save(ReviewModel)
async def generate_slug(sender: "Type[ReviewModel]", instance: ReviewModel,
                        using_db, update_fields: List[str]) -> None:
    if sender is not ReviewModel:
        return

    if not instance.slug and instance.title:
        instance.slug = slugify(str(instance.title).lower())


class RatingReviewModel(Model):
    id = fields.IntField(pk=True)
    mark = fields.SmallIntField(default=0)
    review_id: fields.ForeignKeyRelation['ReviewModel'] = fields.ForeignKeyField(
        "reviews.ReviewModel", null=True, related_name="rating_review"
    )
    owner_id = fields.IntField()

    def __str__(self):
        return f"From {self.owner_id} set rating {self.mark}"


class CommentReviewModel(Model):
    id = fields.IntField(pk=True)
    owner_id = fields.IntField()
    review_id: fields.ForeignKeyRelation['ReviewModel'] = fields.ForeignKeyField(
        "reviews.ReviewModel", null=True, related_name="comment_review", on_delete=fields.SET_NULL
    )
    # parent: fields.ForeignKeyNullableRelation["CommentReviewModel"] = fields.ForeignKeyField(
    # "reviews.CommentReviewModel",
    # on_delete=fields.SET_NULL,
    # null=True,
    # related_name="children")
    text = fields.TextField(max_length=2000)
    created_at = fields.DatetimeField(auto_now_add=True)
    is_deleted = fields.BooleanField(default=False)
    # children: fields.ReverseRelation["CommentReviewModel"]

    class PydanticMeta:
        exclude = ("review_id",)

    def __str__(self):
        return f"In {self.review_id} set comment with text: {self.text}"


Tortoise.init_models(["src.models.reviews"], "reviews")

from tortoise.contrib.pydantic import pydantic_model_creator
from src.models import reviews, news
from datetime import datetime
from pydantic import BaseModel, constr, conint
from typing import List
from src.models.categories import Categories


class ReviewSerializer(BaseModel):
    title: constr(max_length=50)
    description: constr(max_length=1500)
    name_of_the_type: str
    personal_rating: conint(le=10, ge=1)
    owner_id: int


class ShowReviewSerializer(BaseModel):
    id: int
    title: str
    name_of_the_type: str

    class Config:
        orm_mode = True


class CommentReviewSerializer(BaseModel):
    id: int
    owner_id: int
    review_id: int
    text: str
    created_at: datetime
    is_deleted: bool


class CommentChildrenSerializer(CommentReviewSerializer):
    children: List[CommentReviewSerializer]


class RatingReviewSerializer(BaseModel):
    id: int
    mark: conint(le=10, ge=1)
    review_id: int
    owner_id: int


create_tag_pydantic = pydantic_model_creator(news.TagNewsModel, exclude_readonly=True, exclude=("news", ))
get_tag_pydantic = pydantic_model_creator(news.TagNewsModel, exclude=("news", ))


class NewsSerializer(BaseModel):
    id: int
    tag: List[get_tag_pydantic] = []
    category: Categories
    title: str
    text: str
    publish_at: datetime
    image: str = None
    viewed: int
    author: int


class NewsOneSerializer(BaseModel):
    id: int
    tag: List[get_tag_pydantic] = []
    category: Categories
    title: str


news_pydantic = pydantic_model_creator(news.NewsModel, name="News")

review_pydantic = pydantic_model_creator(reviews.ReviewModel, name="Review")
review_pydantic_no_ids = pydantic_model_creator(reviews.ReviewModel, name="ReviewIn", exclude_readonly=True)

comment_review_pydantic = pydantic_model_creator(reviews.CommentReviewModel, name="Comment", exclude_readonly=True,
                                                 exclude=("parent", ))
create_comment_review_pydantic = pydantic_model_creator(reviews.CommentReviewModel, exclude_readonly=True,
                                                        exclude=("owner_id", "is_deleted", ))

rating_review_pydantic = pydantic_model_creator(reviews.RatingReviewModel, name="Rating", exclude_readonly=True)

